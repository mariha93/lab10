(define add (lambda (a b c d) (a c (b c d)))) ;addition function

(define sub (lambda (m n) (n pred m))) ;subtraction function

(define logicand (lambda (m n) (lambda (a b) (n (m a b) b)))) ;AND function

(define logicor (lambda (m n) (lambda (a b) (n a(m a b))))) ;OR function

(define logicnot (lambda (m) (lambda (a b) (m b a)))) ;NOT function

(define iszero (lambda (n) n (lambda (x) (false)) true)) ;iszero function

(define LEQ (lambda (m n)(iszero (sub m n)))) ;LEQ function

(define EQ (lambda (m n) (logicand (LEQ m n) (LEQ n m)))) ;EQ function

(define LT (lambda (m n) (logicand (LEQ m n) (logicnot(EQ m n))))) ;LT function

(define GEQ (lambda (m n) (logicnot (LT m n)))) ;GEQ function



